﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" MadCap:lastBlockDepth="4" MadCap:lastHeight="866" MadCap:lastWidth="651">
    <head>
    </head>
    <body>
        <h1 MadCap:autonum="1. &#160;"><a name="top"></a>Modifying the Inventory Files</h1>
        <p style="text-align: left;">The Ansible Inventory file is used to describe an S3 Connector deployment configuration, including specification of the target servers used for the deployment, and to indicate where the various component services are to be hosted. Ansible is used to orchestrate the federated deployment of the services as per the inventory file, across all of the target servers. </p>
        <p style="text-align: left;">A set of default standard inventory file templates is available to be edited for specific requirements, depending on the number of data centers (sites) in the deployment: </p>
        <ul>
            <li style="text-align: left;">
                <p><a href="https://github.com/scality/Federation/blob/rel/6.4/env/client-template/inventory.1site" title="Single site inventory file template" alt="Single site inventory file template">Single site</a>
                </p>
            </li>
            <li style="text-align: left;">
                <p><a href="https://github.com/scality/Federation/blob/rel/6.4/env/client-template/inventory.2sites" title="2-site stretched inventory file template" alt="2-site stretched inventory file template">2-site stretched</a>
                </p>
            </li>
            <li style="text-align: left;">
                <p> <a href="https://github.com/scality/Federation/blob/rel/6.4/env/client-template/inventory.3sites" title=" 3-site stretched inventory file template" alt=" 3-site stretched inventory file template">3-site stretched</a></p>
            </li>
        </ul>
        <p style="text-align: left;">To declare the machines to which S3 Connector components are to be deployed, use the appropriate inventory file previously copied to the deployment server under <em>env/{{targetEnvironmentName}}</em>. </p>
        <h2 MadCap:autonum="1.1. &#160;">Installation on a Single-Site RING</h2>
        <p style="text-align: left;">Six servers is the required minimum for an <MadCap:variable name="S3 Connector Variables.ComponentName" /> installation on a single-site RING. The S3 Metadata service is hosted on five of the six servers, because it establishes a special cluster with a quorum that works more efficiently with an odd number of servers. </p>
        <p style="text-align: left;">Vault stores its account information in the installed S3 Metadata components, so each installed Vault instance is able to access account information created by a companion Vault component.</p>
        <p class="Note" style="text-align: left;"><span class="ProperNameBold"><a href="https://github.com/scality/Federation/blob/rel/7.0/env/client-template/inventory.1site">client-template/inventory.1site</a></span> (GitHub login required) is the only inventory file template to use in the installation of <MadCap:variable name="S3 Connector Variables.ComponentName" /> on a single-site RING.</p>
        <h2 MadCap:autonum="1.2. &#160;"><a name="Installa"></a>Installation on Stretched RINGs for 2 or 3 Sites</h2>
        <p style="text-align: left;">The <MadCap:variable name="S3 Connector Variables.ComponentName" /> supports deployment across either 2 or 3 data centers (sites) using a stretched model. The goal of the stretched deployment model is to continue service and data availability even in the event of a data center outage, and to protect the data in the event of a site outage. Some of these availability properties, data protection schemes, storage overhead, and failover mechanisms differ between 2-site and 3-site deployments. For information on administrator actions pertaining to site failover, refer to  the <MadCap:variable name="General.Operations Guide" />.</p>
        <p style="text-align: left;">The stretched model uses a single data RING, composed of machines distributed and accessed across the sites. Data is protected using both a distributed erasure coding and replication scheme according to best-practices for 2-site and 3-site data durability requirements.</p>
        <p style="text-align: left;">In these stretched deployments, the S3 Metadata service is also distributed across servers on the two sites. Because the metadata service is run as a special cluster of 5 servers, the metadata distribution scheme is non-uniform across the sites, as described below. The 2-site and 3-site inventory template files described above can serve as the basis of deploying these stretched modes, but will require some editing and customization for specific site requirements. Please consult a Scality Customer Support Engineer (CSE) for further details and assistance in editing the template files. </p>
        <p class="NoteUnder" style="text-align: left;">The stretched deployment must have a high-speed, low-latency (&lt; 10 ms) network connection between the sites. See <MadCap:xref href="../1_Introduction/Multi-Site Deployments.htm">"Multi-Site Deployments" on page&#160;1</MadCap:xref>.</p>
        <p style="text-align: left;">With five servers across the 2 or 3 sites, the Metadata service has the resilience to automatically recover and maintain availability from one or two simultaneous server failures. The service automatically reassigns a metadata server and the metadata leader during nominal and failover operations and maintains consistency across all active servers. The stretched configurations also ensure that the service can be continued either automatically or through administrator initiated recovery procedures after a site failure, as described fully in the <span class="PublicationName"><MadCap:variable name="General.Operations Guide"></MadCap:variable></span>.</p>
        <h2 MadCap:conditions="" MadCap:autonum="1.3. &#160;">Template Changes for Kibana and COSBench</h2>
        <p MadCap:conditions="" style="text-align: left;">To use Kibana for analysis and visualization of the log files, assign port 5601 on the logger machines in the Kibana setup. </p>
        <p MadCap:conditions="" style="page-break-after: avoid;text-align: left;">Optionally, to measure cloud storage performance, install the COSBench controller on a separate logger machine by uncommenting the <i>cosbench_controller</i> section in the template file (making sure to replace "example.com" with the DNS or IP address of the logger machine). </p>
        <p class="codeparatext" MadCap:conditions="">#[cosbench_controller]</p>
        <p class="codeparatext_lastline" MadCap:conditions="">#logger.example.com</p>
        <p MadCap:conditions="" style="text-align: left;">Next, go to <span class="HTMLCode" style="font-style: italic;">http://logger.example.com:19088/controller/index.html</span> to open the controller Web UI.</p>
        <p MadCap:conditions="" style="page-break-after: avoid;text-align: left;">To set up endpoints for the COSBench workload, uncomment the <span class="Code_Terminal" style="font-style: italic;">cosbench_drivers</span> section in the template file and replace the examples with the server DNS names or IP addresses:</p><pre MadCap:conditions="" xml:space="preserve">#[cosbench_drivers]
#server1.example.com
#server2.example.com
#server3.example.com
#server4.example.com
#server5.example.com</pre>
        <p MadCap:conditions="" style="text-align: left;"> By placing cosbench drivers on the same machines as the S3 runners,  localhost can be used as the endpoints for the cosbench workload (instead of mapped DNS names or IP addresses). This is possible because Ansible links the S3 runners section with the <span class="Code_Terminal" style="font-style: italic;">cosbench_drivers</span> section of the inventory file.</p>
    </body>
</html>